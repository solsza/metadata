package so.meta.metadata.domain.file;

import java.util.regex.Pattern;
import lombok.extern.java.Log;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

@Log
public class FileValidator {

    public static Boolean fileNameValidation(MultipartFile file) {
        String fileNameTMP = StringUtils.cleanPath(file.getOriginalFilename());
        final Pattern pattern = Pattern.compile("^[a-zA-ZżźćńółęąśŻŹĆĄŚĘŁÓŃ0-9_@-]+(\\.doc|\\.DOC|\\.docx|\\.DOCX|\\.xls|\\.XLS|\\.xlsx|\\.XLSX|\\.jpg|\\.jpeg|\\.pdf|\\.PDF|\\.odt|\\.ODT|\\.ods|\\.ODS|\\.png|\\.PNG|\\.bmp|\\.BMP)$");
        if (!pattern.matcher(fileNameTMP).matches()) {
            log.info("Filename " + fileNameTMP + " is incorrect");
            return true;
        }
        return false;
    }
}
