package so.meta.metadata.domain.file;

import java.util.Arrays;

public interface TypeOfFile {

    public boolean belongs (String value);
    public String getRemoverClassName();
}
