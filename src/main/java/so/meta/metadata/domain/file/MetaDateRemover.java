package so.meta.metadata.domain.file;

import java.io.File;

public interface MetaDateRemover {

    void deleteMetadataFromFile(File file);

}
