package so.meta.metadata.domain.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FileStorageService {

    private final Path fileStorageLocation;
    private final FileTypeDistributor fileTypeDistributor;

    @Autowired
    public FileStorageService(FileStorageProperties fileStorageProperties, FileTypeDistributor fileTypeDistributor) {
        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
            .toAbsolutePath().normalize();
        this.fileTypeDistributor = fileTypeDistributor;
        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new FileStorageException("Could not create the storage directory", ex);
        }
    }

    public String removeMetadataFromFile(MultipartFile multipartFile) {

        File file = storeFile(multipartFile);
        MetaDateRemover metaDateRemover = fileTypeDistributor.distributeFileByType(file, file.getName());
        metaDateRemover.deleteMetadataFromFile(file);
        return file.getName();
    }

    private File storeFile(MultipartFile file) {

        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        Path targetLocation = this.fileStorageLocation.resolve(fileName);
        if (FileValidator.fileNameValidation(file)) {
            throw new FileStorageException("Filename contains invalid path sequence " + fileName);
        }
        copyFileToLocalDirectory(file, targetLocation);
        return new File(String.valueOf(targetLocation));
    }

    private void copyFileToLocalDirectory(MultipartFile file, Path targetLocation) {
        try {
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
