package so.meta.metadata.domain.file;

import java.io.File;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.stereotype.Component;

@Log
@Component
@RequiredArgsConstructor
public class FileTypeDistributor {

    private final Map<String, MetaDateRemover> mapOfBeans;

    public MetaDateRemover distributeFileByType(File file, String fileName) {
        String removerClassName ="";
        String fileType = fileName.split("\\.")[1];

        for (FileTypesEnum fileTypesEnum : FileTypesEnum.values()) {
            if(fileTypesEnum.belongs(fileType)){
                removerClassName = fileTypesEnum.getRemoverClassName();
            }
        }
        if(mapOfBeans.containsKey(removerClassName)){
            return mapOfBeans.get(removerClassName);
        }
        else {
            throw new FileStorageException("Wrong File type");
        }
    }
}
