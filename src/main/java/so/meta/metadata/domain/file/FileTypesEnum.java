package so.meta.metadata.domain.file;

import java.util.Arrays;

public enum FileTypesEnum implements TypeOfFile {

    OLDOFFICE (".doc.DOC.xls.XLS", "officeOldVersionFileMetadataRemover"),
    NEWOFFICE (".docx.DOCX.xlsx.XLSX", "officeNewVersionFileMetadataRemover"),
    GRAPHICJPG(".jpeg.jpg.JPG.JPEG","jpgFileRemover"),
    GRAPHICPNG (".png.PNG" , "pngFileRemover"),
    GRAPHICBMP (".bmp.BMP","bmpFileRemover"),
    PDF (".pdf.PDF", "pdfFileRemover"),
    OPENOFFICE (".odt.ODT.ods.ODS", "openOfficeRemover");

    private String types;
    private String metaDataService;

    FileTypesEnum(String types, String metaDataService) {
        this.types = types;
        this.metaDataService = metaDataService;
    }

    public String getMetaDataService() {
        return metaDataService;
    }
    public boolean belongs(String value){
        String[] split = this.types.split("\\.");
        boolean checker = Arrays.stream(split).anyMatch(splitValue -> splitValue.equals(value));
        return checker;
    }

    @Override
    public String getRemoverClassName() {
        return this.getMetaDataService();
    }


}
