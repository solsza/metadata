package so.meta.metadata.domain.graphic;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.ImageWriteException;
import org.apache.commons.imaging.Imaging;
import org.apache.commons.imaging.formats.jpeg.JpegImageMetadata;
import org.apache.commons.imaging.formats.jpeg.exif.ExifRewriter;
import org.apache.commons.imaging.formats.tiff.TiffImageMetadata;
import org.apache.commons.imaging.formats.tiff.constants.ExifTagConstants;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputDirectory;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputField;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputSet;
import org.springframework.stereotype.Service;
import so.meta.metadata.domain.file.MetaDateRemover;

@Service
@RequiredArgsConstructor
@Log
public class JpgFileRemover implements MetaDateRemover {

    @Override
    public void deleteMetadataFromFile(File file) {

//        try {
//            TiffOutputSet outset = null;
//            JpegImageMetadata jpegImageMetadata = (JpegImageMetadata) Imaging.getMetadata(file);
//            if (jpegImageMetadata != null) {
//                TiffImageMetadata tiffImageMetadata = jpegImageMetadata.getExif();
//                if (tiffImageMetadata != null) {
//                    outset = tiffImageMetadata.getOutputSet();
//                }
//            }
//            if (outset == null) {
//               outset = new TiffOutputSet();
//            }
//
//            TiffOutputDirectory tiffOutputDirectory = outset.getOrCreateExifDirectory();
//            //changing fields
//            tiffOutputDirectory.removeField(ExifTagConstants.EXIF_TAG_APERTURE_VALUE);
//            tiffOutputDirectory.removeField(ExifTagConstants.EXIF_TAG_CAMERA_OWNER_NAME);
//            tiffOutputDirectory.removeField(ExifTagConstants.EXIF_TAG_GPSINFO);
//            tiffOutputDirectory.removeField(ExifTagConstants.EXIF_TAG_BODY_SERIAL_NUMBER);
//            outset.setGPSInDegrees(90d, 90d);
//
//
//            List<TiffOutputField> fields = tiffOutputDirectory.getFields();
//            for (TiffOutputField tiff : fields
//            ) {
//                log.info(tiff.toString() + tiff.fieldType.toString()) ;
//            }
            File outPutFile  = new File("D:\\JAVA\\metadata\\src\\main\\resources\\myfiles\\" + "001" + file.getName());
//
//            updateExifMetadata(file,outPutFile,outset);
//            Files.delete(file.toPath());

//            remove all metaData
            removeExifMetadata(file, outPutFile);
        try {
            Files.delete(file.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
//        } catch (ImageReadException | IOException | ImageWriteException e) {
//            e.printStackTrace();
//        }
    }

    public void removeExifMetadata(final File file, final File dst) {

        try (FileOutputStream fos = new FileOutputStream(dst);
            OutputStream os = new BufferedOutputStream(fos)) {
            new ExifRewriter().removeExifMetadata(file, os);
        } catch (ImageReadException | ImageWriteException | IOException exception) {
            exception.printStackTrace();
        }
    }

    public void updateExifMetadata(final File file, final File dst, TiffOutputSet outputSet) {

        try (FileOutputStream fos = new FileOutputStream(dst);
            OutputStream os = new BufferedOutputStream(fos)) {
            new ExifRewriter().updateExifMetadataLossless(file, os ,outputSet);
        } catch (ImageReadException | ImageWriteException | IOException exception) {
            exception.printStackTrace();
        }
    }

}
