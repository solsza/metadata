package so.meta.metadata.domain.graphic;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.stream.ImageOutputStream;
import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.ImageWriteException;
import org.apache.commons.imaging.formats.jpeg.exif.ExifRewriter;
import org.springframework.stereotype.Service;
import so.meta.metadata.domain.file.MetaDateRemover;

@Service
public class BmpFileRemover implements MetaDateRemover {

    @Override
    public void deleteMetadataFromFile(File file) {
        ImageReader reader = ImageIO.getImageReadersBySuffix("bmp").next();
        try {
            reader.setInput(ImageIO.createImageInputStream(file));
            //save metadata
            IIOMetadata metadata = reader.getImageMetadata(0);
            //put image in bi
            BufferedImage bi = reader.read(0);

            File outPutFile  = new File("C:\\Users\\SO\\Desktop\\prywatne\\PROGRAMOWANIE\\metadata\\src\\main\\resources\\myfiles\\" + "test" + file.getName());
            FileOutputStream fos = new FileOutputStream(outPutFile);
            OutputStream os = new BufferedOutputStream(fos);

            ImageOutputStream ios = ImageIO.createImageOutputStream(os);
            Iterator<ImageWriter> iter = ImageIO.getImageWritersByFormatName("bmp");
            ImageWriter writer = iter.next();
            writer.setOutput(ios);
            ImageWriteParam imageWriteParam = writer.getDefaultWriteParam();
            writer.write(null,new IIOImage(bi,null,null),imageWriteParam);
            writer.dispose();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}