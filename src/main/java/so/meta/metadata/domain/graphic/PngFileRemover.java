package so.meta.metadata.domain.graphic;

import ar.com.hjg.pngj.IImageLine;
import ar.com.hjg.pngj.ImageInfo;
import ar.com.hjg.pngj.PngReader;
import ar.com.hjg.pngj.PngWriter;
import ar.com.hjg.pngj.chunks.ChunkCopyBehaviour;
import ar.com.hjg.pngj.chunks.PngMetadata;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.RenderedImage;
import java.awt.image.WritableRaster;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.metadata.IIOMetadataNode;
import javax.imageio.stream.ImageInputStream;
import org.apache.commons.imaging.formats.png.PngImageParser;
import org.springframework.stereotype.Service;
import so.meta.metadata.domain.file.MetaDateRemover;
//TO DO make tests for external sources
@Service
public class PngFileRemover implements MetaDateRemover {

    @Override
    public void deleteMetadataFromFile(File file) {

        PngReader pngReader = new PngReader(file);
        PngMetadata metadata = pngReader.getMetadata();
        File outPutFile  = new File("D:\\JAVA\\metadata\\src\\main\\resources\\myfiles\\" + "001" + file.getName());
        PngWriter pngWriter = new PngWriter(outPutFile, pngReader.imgInfo,true);
        pngWriter.copyChunksFrom(pngReader.getChunksList(), ChunkCopyBehaviour.COPY_NONE);
        pngWriter.getMetadata().setText("testSO","testSOValue"); // for test
        for(int row =0; row< pngWriter.imgInfo.rows; row++){
            IImageLine l1 = pngReader.readRow();
            pngWriter.writeRow(l1);
        }
        pngReader.end();
        pngWriter.end();

//        BufferedImage image;
//        try {
//            ImageInputStream iis = ImageIO.createImageInputStream(file);
//            Iterator<ImageReader> readers = ImageIO.getImageReaders(iis);
//
//            if (readers.hasNext()) {
//                ImageReader imageReader = readers.next();
//                imageReader.setInput(iis, true);
//                IIOMetadata metadata = imageReader.getImageMetadata(0);
//                String[] names = metadata.getMetadataFormatNames();
//                int length = names.length;
//                for (int i = 0; i < length; i++) {
//                    System.out.println("Format name: " + names[i]);
//                }
//
//                IIOMetadataNode textEntry = new IIOMetadataNode("testBrafTech");
//
//            }

//            BufferedImage bufferedImage = ImageIO.read(file);
//            WritableRaster raster = bufferedImage.getRaster();
//            DataBufferByte data = (DataBufferByte) raster.getDataBuffer();
//
//            ImageReader imageReader = ImageIO.getImageReadersByFormatName("png").next();
//            imageReader.setInput(, true);
//
//            //read data from first image
//            IIOMetadata metadata  = imageReader.getImageMetadata(0);
//            String[] names = metadata.getMetadataFormatNames();

//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

}
