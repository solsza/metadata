package so.meta.metadata.domain.office;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import lombok.extern.java.Log;
import org.apache.poi.hpsf.CustomProperties;
import org.apache.poi.hpsf.DocumentSummaryInformation;
import org.apache.poi.hpsf.NoPropertySetStreamException;
import org.apache.poi.hpsf.Property;
import org.apache.poi.hpsf.PropertySet;
import org.apache.poi.hpsf.PropertySetFactory;
import org.apache.poi.hpsf.SummaryInformation;
import org.apache.poi.hpsf.UnexpectedPropertySetTypeException;
import org.apache.poi.hpsf.WritingNotSupportedException;
import org.apache.poi.poifs.filesystem.DirectoryEntry;
import org.apache.poi.poifs.filesystem.DocumentEntry;
import org.apache.poi.poifs.filesystem.DocumentInputStream;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.springframework.stereotype.Service;
import so.meta.metadata.domain.file.MetaDateRemover;

@Service
@Log
//to refactor
public class OfficeOldVersionFileMetadataRemover implements MetaDateRemover {

    public void deleteMetadataFromFile(File file) {
        POIFSFileSystem poiFile = createNewPOIFSFile(file);
        saveModifiedFile(changeFileMetadataForOldOfficeFile(poiFile), file);
    }

    private POIFSFileSystem createNewPOIFSFile(File file) {
        POIFSFileSystem poiFile = createPOIFile(file);
        return poiFile;
    }

    private POIFSFileSystem changeFileMetadataForOldOfficeFile(POIFSFileSystem poiFile) {

        DirectoryEntry directoryEntry = poiFile.getRoot();
        SummaryInformation summaryInformation;
        DocumentSummaryInformation documentSummaryInformation;

        try {
            DocumentEntry documentEntryForSummaryInformation = (DocumentEntry) directoryEntry.getEntry(SummaryInformation.DEFAULT_STREAM_NAME);
            DocumentEntry documentEntryForDocumentSummaryInformation = (DocumentEntry) directoryEntry.getEntry(DocumentSummaryInformation.DEFAULT_STREAM_NAME);

            DocumentInputStream documentInputStreamForSI = new DocumentInputStream(documentEntryForSummaryInformation);
            PropertySet propertySetSI = new PropertySet(documentInputStreamForSI);
            documentInputStreamForSI.close();

            DocumentInputStream documentInputStreamForDSI = new DocumentInputStream(documentEntryForDocumentSummaryInformation);
            PropertySet propertySetForDSI = new PropertySet(documentInputStreamForDSI);
            documentInputStreamForDSI.close();

            summaryInformation = new SummaryInformation(propertySetSI);
            summaryInformation.setAuthor("SO");
            summaryInformation.setTitle("BrafTech");
            summaryInformation.setLastAuthor("BrafTech");

            Property[] summaryInformationProperties = summaryInformation.getProperties();
            Arrays.stream(summaryInformationProperties).forEach(documentProperty ->log.info(documentProperty.toString() + documentProperty.getValue().toString()));

            documentSummaryInformation = new DocumentSummaryInformation(propertySetForDSI);
            documentSummaryInformation.setCompany("BrafTech");
            CustomProperties customProperties = new CustomProperties();
            customProperties.put("BrafTech", "BrafTech");
            documentSummaryInformation.setCustomProperties(customProperties);

            Property[] documentSummaryInformationProperties = documentSummaryInformation.getProperties();
            Arrays.stream(documentSummaryInformationProperties).forEach(property -> log.info(property.toString() + property.getValue().toString()));


        } catch (UnexpectedPropertySetTypeException | NoPropertySetStreamException | IOException exception) {
            exception.printStackTrace();

            summaryInformation = PropertySetFactory.newSummaryInformation();
            documentSummaryInformation = PropertySetFactory.newDocumentSummaryInformation();
        }

        try {
            documentSummaryInformation.write(directoryEntry, SummaryInformation.DEFAULT_STREAM_NAME);
            summaryInformation.write(directoryEntry, SummaryInformation.DEFAULT_STREAM_NAME);

        } catch (WritingNotSupportedException | IOException e) {
            e.printStackTrace();
        }
        return poiFile;
    }

    private void saveModifiedFile(POIFSFileSystem poifsFileSystem, File file) {
        try {
            OutputStream outputStream = new FileOutputStream(file);
            poifsFileSystem.writeFilesystem(outputStream);
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private POIFSFileSystem createPOIFile(File file) {
        POIFSFileSystem poifsFileSystem = null;
        try {
            InputStream inputStream = new FileInputStream(file);
            poifsFileSystem = new POIFSFileSystem(inputStream);
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return poifsFileSystem;
    }
}
