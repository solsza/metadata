package so.meta.metadata.domain.office;

import java.io.File;
import java.io.IOException;
import lombok.extern.java.Log;
import org.apache.poi.ooxml.POIXMLProperties;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.xmlbeans.XmlException;
import org.springframework.stereotype.Service;
import so.meta.metadata.domain.file.MetaDateRemover;

@Service
@Log
public class OfficeNewVersionFileMetadataRemover implements MetaDateRemover {

    public void deleteMetadataFromFile(File file) {
        try {
            OPCPackage pkg = OPCPackage.open(file);
            POIXMLProperties props = new POIXMLProperties(pkg);
            props.getCoreProperties();
            props.getExtendedProperties();
            props.getCustomProperties();
            props.getCoreProperties().setTitle("MYTitle1");
            props.getExtendedProperties().setCompany("BrafTech1");
            props.commit();
            pkg.close();
        } catch (XmlException | OpenXML4JException | IOException e) {
            e.printStackTrace();
        }
    }


}
