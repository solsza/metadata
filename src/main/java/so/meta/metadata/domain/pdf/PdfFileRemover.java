package so.meta.metadata.domain.pdf;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import lombok.extern.java.Log;
import org.springframework.stereotype.Service;
import so.meta.metadata.domain.file.MetaDateRemover;

@Service
@Log
public class PdfFileRemover implements MetaDateRemover {

    @Override
    public void deleteMetadataFromFile(File file) {

        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(file);
            PdfReader reader1 = new PdfReader(inputStream);
            inputStream.close();
            File outPutFile  = new File("D:\\JAVA\\metadata\\src\\main\\resources\\myfiles\\" + "001" + file.getName());
            FileOutputStream fos = new FileOutputStream(outPutFile);
            OutputStream os = new BufferedOutputStream(fos);
            PdfStamper pdfStamper = new PdfStamper(reader1, os);

            PdfReader pdfReader = pdfStamper.getReader();
            pdfReader.getCatalog().remove(PdfName.METADATA);
            pdfReader.removeUnusedObjects();

            HashMap<String, String> info = pdfReader.getInfo();
            info.put("Title", null);
            info.put("Author", null);
            info.put("Subject", null);
            info.put("Keywords", null);
            info.put("Creator", null);
            info.put("Producer", null);
            info.put("CreationDate", null);
            info.put("ModDate", null);
            info.put("Trapped", null);

            pdfStamper.setMoreInfo(info);
            pdfStamper.close();

        } catch (IOException | DocumentException e) {
            e.printStackTrace();
        }
    }
}
