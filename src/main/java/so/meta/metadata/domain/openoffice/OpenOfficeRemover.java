package so.meta.metadata.domain.openoffice;


import java.io.File;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import org.odftoolkit.odfdom.doc.OdfDocument;
import org.odftoolkit.odfdom.incubator.meta.OdfOfficeMeta;
import org.springframework.stereotype.Service;
import so.meta.metadata.domain.file.MetaDateRemover;

@Service
public class OpenOfficeRemover implements MetaDateRemover {

    @Override
    public void deleteMetadataFromFile(File file) {
        try {
            OdfDocument odfDocument = OdfDocument.loadDocument(file);
            OdfOfficeMeta officeMetadata = odfDocument.getOfficeMetadata();
            officeMetadata.setTitle("test");
            officeMetadata.setDescription("test");
            officeMetadata.setCreator("test");
            officeMetadata.setDate(Calendar.getInstance());
            officeMetadata.setDescription("test");
            officeMetadata.setCreationDate(Calendar.getInstance());
            officeMetadata.setPrintDate(Calendar.getInstance());
            officeMetadata.setPrintedBy("Test");
            officeMetadata.setInitialCreator("Test");

            List<String> emptyKeywords = new LinkedList<>();
            emptyKeywords.add("test");

            List<String> keywords = officeMetadata.getKeywords();
            if (keywords != null) {
                officeMetadata.setKeywords(emptyKeywords);
            }

            List<String> userDefinedDataNames = officeMetadata.getUserDefinedDataNames();
            if (userDefinedDataNames != null) {
                for (String userDefine : userDefinedDataNames) {
                    officeMetadata.removeUserDefinedDataByName(userDefine);
                }
            }

            odfDocument.save(new File("D:\\JAVA\\metadata\\src\\main\\resources\\myfiles\\" + "001" + file.getName()));
            odfDocument.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}